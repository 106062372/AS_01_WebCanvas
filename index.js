// Canvas DOM 元素 
const canvas = document.getElementById('canvas')
const ctx = canvas.getContext('2d')
var f1 = 0; var f2 = 0; var f3 = 0; var f4 = 0; var f5 = 0; var f6 = 0; var f7 = 0; var f8 = 0; var f9 = 0;


function brush() {
    if (!f1) {
        f1 = 1; f2 = 0; f3 = 0; f4 = 0; f5 = 0; f6 = 0; f7 = 0; f8 = 0; f9 = 0;
        //console.log("brush")
        document.getElementById("canvas").style.cursor = "url(image/brush.png),default"
        ctx.globalCompositeOperation = "source-over";
        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false

        canvas.addEventListener('mousedown', function (e) {
            if (f1 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY

                ctx.strokeStyle = document.getElementById("favcolor").value

                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f1 == 1) {
                if (!isMouseActive) {
                    return
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                ctx.beginPath()
                ctx.moveTo(pos_x1, pos_y1)
                ctx.lineTo(pos_x2, pos_y2)
                ctx.stroke()
                pos_x1 = pos_x2
                pos_y1 = pos_y2
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f1 == 1) {
                //console.log("hi")
                Push()
                isMouseActive = false
            }
        })
    }
}

function eraser() {
    if (!f2) {
        f1 = 0; f2 = 1; f3 = 0; f4 = 0; f5 = 0; f6 = 0; f7 = 0; f8 = 0; f9 = 0;
        //console.log("eraser")
        document.getElementById("canvas").style.cursor = "url(image/eraser.png),default"
        ctx.globalCompositeOperation = "destination-out";

        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false
        canvas.addEventListener('mousedown', function (e) {
            if (f2 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY
                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f2 == 1) {
                if (!isMouseActive) {
                    return
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                ctx.beginPath()
                ctx.moveTo(pos_x1, pos_y1)
                ctx.lineTo(pos_x2, pos_y2)
                ctx.stroke()
                pos_x1 = pos_x2
                pos_y1 = pos_y2
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f2 == 1) {
                Push()
                isMouseActive = false
            }
        })
    }
}

function text() {
    if (!f3) {
        //console.log("text")
        document.getElementById("canvas").style.cursor = "url(image/text.png),default"
        f1 = 0; f2 = 0; f3 = 1; f4 = 0; f5 = 0; f6 = 0; f7 = 0; f8 = 0; f9 = 0;
        ctx.lineWidth = 0

        var posx = 0;
        var posy = 0;
        canvas.addEventListener('click', function (e) {
            if (f3 == 1) {
                //console.log("type_in")
                posx = e.offsetX;
                posy = e.offsetY;
                //console.log("x coords: " + posx + ", y coords: " + posy);
            }
        })
        canvas.removeEventListener('click', function (e) {
            if (f3 == 1) {
                //console.log("type_out")
                posx = e.offsetX;
                posy = e.offsetY;
                //console.log("x coords: " + posx + ", y coords: " + posy);
            }
        })
        ctx.globalCompositeOperation = "source-over";
        var word_ = ""
        window.addEventListener('keydown', function (e) {
            if (f3 == 1) {
                if (e.key == "Backspace") {
                    if (word_ != "") {
                        word_ = word_.substring(0, word_.length - 1);
                    }
                }
                else if (e.key == "Enter") {
                    ctx.font = document.getElementById("font-size").value + 'px ' + document.getElementById("font").value; /// using the last part
                    ctx.fillStyle = document.getElementById("favcolor").value
                    ctx.fillText(document.getElementById("word").value, posx, posy);
                    word_ = ""
                    Push()
                }
                else {
                    word_ = word_ + e.key;
                }
                document.getElementById("word").value = word_;
            }
        })
    }
}

function refresh() {
    if (!f4) {
        f1 = 0; f2 = 0; f3 = 0; f4 = 1; f5 = 0; f6 = 0; f7 = 0; f8 = 0; f9 = 0;
        document.getElementById("canvas").style.cursor = "default";
        //console.log("refresh")
        Push()
        if (confirm("Are you sure you want to refresh?")) {
            ctx.clearRect(0, 0, canvas.width, canvas.height)
        }
        step = 0;
    }
}

function download() {
    var download = document.getElementById("download");
    var image = document.getElementById("canvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
}


function readImage() {
    if (this.files && this.files[0]) {
        var fr = new FileReader()
        fr.onload = function (e) {
            var img = new Image()
            img.addEventListener("load", function () {
                ctx.drawImage(img, 0, 0)
            })
            img.src = e.target.result;
        }
        fr.readAsDataURL(this.files[0])
    }
    Push()
}
document.getElementById("upload").addEventListener("change", readImage, false)



function triangle() {
    if (!f6) {
        f1 = 0; f2 = 0; f3 = 0; f4 = 0; f5 = 0; f6 = 1; f7 = 0; f8 = 0; f9 = 0;
        //console.log("triangle")
        document.getElementById("canvas").style.cursor = "url(image/triangle.png),default"
        ctx.globalCompositeOperation = "source-over";
        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false

        canvas.addEventListener('mousedown', function (e) {
            if (f6 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY

                ctx.strokeStyle = document.getElementById("favcolor").value
                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
                Push()
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f6 == 1) {
                if (!isMouseActive) {
                    return
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                let diff_x = pos_x2 - pos_x1
                let diff_y = pos_y2 - pos_y1

                Undo()
                ctx.beginPath()
                ctx.moveTo(pos_x1, pos_y1)
                ctx.lineTo(pos_x2, pos_y2)
                ctx.lineTo(pos_x1 - diff_x, pos_y1 + diff_y)
                ctx.closePath()//把線連起來
                ctx.stroke()
                Push()
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f6 == 1) {
                isMouseActive = false
            }
        })
    }
}


function circle() {
    if (!f7) {
        f1 = 0; f2 = 0; f3 = 0; f4 = 0; f5 = 0; f6 = 0; f7 = 1; f8 = 0; f9 = 0;
        //console.log("circle")
        document.getElementById("canvas").style.cursor = "url(image/circle.png),default"
        ctx.globalCompositeOperation = "source-over";
        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false

        canvas.addEventListener('mousedown', function (e) {
            if (f7 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY

                ctx.strokeStyle = document.getElementById("favcolor").value
                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
                Push()
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f7 == 1) {
                if (!isMouseActive) {
                    return
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                let radius = (((pos_x1 - pos_x2) ** 2 + (pos_y1 - pos_y2) ** 2) ** (1 / 2)) / 2;
                Undo()
                ctx.beginPath()
                ctx.arc(pos_x1, pos_y1, radius, (Math.PI * 0) / 180, (Math.PI * 360) / 180);
                ctx.stroke()
                Push()
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f7 == 1) {
                isMouseActive = false
            }
        })
    }
}


function rectangle() {
    if (!f8) {
        f1 = 0; f2 = 0; f3 = 0; f4 = 0; f5 = 0; f6 = 0; f7 = 0; f8 = 1; f9 = 0;
        //console.log("rectangle")
        document.getElementById("canvas").style.cursor = "url(image/rectangle.png),default"
        ctx.globalCompositeOperation = "source-over";
        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false

        canvas.addEventListener('mousedown', function (e) {
            if (f8 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY

                ctx.strokeStyle = document.getElementById("favcolor").value
                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
                Push()
                //console.log(pos_x1+","+pos_y1)
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f8 == 1) {
                if (!isMouseActive) {
                    return
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                ctx.beginPath()
                Undo()
                ctx.moveTo(pos_x1, pos_y1)
                ctx.lineTo(pos_x2, pos_y1)
                ctx.lineTo(pos_x2, pos_y2)
                ctx.lineTo(pos_x1, pos_y2)
                ctx.lineTo(pos_x1, pos_y1)
                ctx.stroke();
                Push()
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f8 == 1) {
                isMouseActive = false
                //console.log(pos_x2+","+pos_y2)
            }
        })
    }
}



var PushArray = new Array();
var step = -1;

function Push() {
    step = step + 1;
    var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    PushArray[step] = imgData;
    //console.log("push")
    //console.log(step)
}

function Undo() {
    if (step > 0) {
        step = step - 1;
        ctx.clearRect(0, 0, canvas.with, canvas.height);
        var imgData = PushArray[step];
        ctx.putImageData(imgData, 0, 0);
    } else {
        step = -1;
        ctx.clearRect(0, 0, canvas.width, canvas.height)
    }
    //console.log("undo")
    //console.log(step)
}

function Redo() {
    if (step < PushArray.length - 1) {
        step = step + 1;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var imgData = PushArray[step];
        ctx.putImageData(imgData, 0, 0);
    }
    //console.log("redo")
    //console.log(step)
}


function rainbow() {
    if (!f9) {
        f1 = 0; f2 = 0; f3 = 0; f4 = 0; f5 = 0; f6 = 0; f7 = 0; f8 = 0; f9 = 1;
        //console.log("rainbow")
        document.getElementById("canvas").style.cursor = "url(image/rainbow.png),default"
        ctx.globalCompositeOperation = "source-over";
        let pos_x1 = 0
        let pos_y1 = 0
        let pos_x2 = 0
        let pos_y2 = 0
        let isMouseActive = false
        let color = 0

        canvas.addEventListener('mousedown', function (e) {
            if (f9 == 1) {
                isMouseActive = true
                pos_x1 = e.offsetX
                pos_y1 = e.offsetY
                ctx.lineWidth = document.getElementById("size").value
                ctx.lineCap = 'round'
                ctx.lineJoin = 'round'
            }
        })

        canvas.addEventListener('mousemove', function (e) {
            if (f9 == 1) {
                if (!isMouseActive) {
                    return
                }
                ctx.strokeStyle = 'hsl(' + color + ', 100%, 60%)';
                color++;
                //console.log("hue")
                if (color >= 360) {
                    color = 0;
                }
                pos_x2 = e.offsetX
                pos_y2 = e.offsetY
                ctx.beginPath()
                ctx.moveTo(pos_x1, pos_y1)
                ctx.lineTo(pos_x2, pos_y2)
                ctx.stroke()
                pos_x1 = pos_x2
                pos_y1 = pos_y2
            }
        })

        canvas.addEventListener('mouseup', function (e) {
            if (f9 == 1) {
                Push()
                isMouseActive = false
            }
        })
    }


}
