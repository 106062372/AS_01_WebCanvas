# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.

![](https://i.imgur.com/C5oJ7ZJ.jpg)
這是我實作出的小畫家，左側為畫布，右上方有四個欄位由上至下為：
顏色、工具大小(筆刷、橡皮擦)、字型、字體大小。
接下來是按鈕，想要哪一種功能點擊對應的按鈕即可。
依照圖片上的順序為：
* 筆刷、橡皮擦、文字
* 重新開始、下一步、上一步
* 三角形、圓形、矩形
* 下載圖片、彩虹筆（新增）
* 上傳圖片

操作上稍微不同的地方為文字功能：點擊圖案為T的按鈕，接著在畫布上點擊想寫字的地方，輸入文字（但不會同步顯示文字），最後按下enter才會顯示出輸入的文字。



### Function description

    Describe your bonus function and how to use it.
**html:**
在不同的button當onclick時，就會到javascript對應的功能。且還有一些功能的選項，例如字體大小、顏色(type=color)等等，都可以在javascript中被讀取做使用。至於排版部分就不多作介紹了，在此說明各功能實作方法。

基本上這次作業會需要用到一個函式為addEventListener()，不過我把addEventListener()寫在每一個function內，雖然可以用一些方式避免掉切換功能時產生出多個event，達成預期功能，但始終這是一個不太正確的寫法。我寫到後來時才發現這個問題，時間上很難允許我將整份程式碼改回正確的方式，還請助教多見諒。

**css:**
canvas和button的圖片，還有button的一些小功能。

**javascript:**
透過ctx = canvas.getContext('2d')就可以取得渲染環境及其繪圖函數。

brush()：
將過程分成三個狀態：mousedown、mousemove、mouseup
mousedown：紀錄滑鼠點擊在畫布上的位置(e.offset)，且初始化筆刷顏色(ctx.strokeStyle)和大小(ctx.lineWidth)，並設立一個變數來判斷是不是正在畫(isMouseActive)。
mousemove：ctx.beginPath()產生一個新路徑，ctx.lineTo再從第一個位置畫線到第二個位置，最後ctx.stroke()將圖畫出來。因為移動過程中都會觸發此event，所以會持續更新畫出的圖
mouseup：isMouseActive變數設為0

eraser()：
大致上跟brush差不多，不一樣的地方在於合成效果。brush為"source-over"，代表將新圖形畫在舊圖形之上。eraser為"destination-out"，代表只保留新、舊圖形非重疊的舊圖形區域，其餘皆變為透明。這樣一來就可以達成eraser功能。

text()：
點擊畫布上想要寫上文字的地方，當keydown時輸入文字，不過不會立即顯示在畫布上，最後輸入enter後才會顯示輸入文字。可以藉由ctx.font字體大小及字型，ctx.fillText將文字顯示在畫布上。

Cursor icon：
在點擊到某button時，游標圖示會與功能的icon相同。做法為在不同功能的function中加入
```
document.getElementById("canvas").style.cursor = "url(image/某function.png),default"
```
就可以成功將游標的圖示改變。

refresh()：
使用ctx.clearRect()將範圍調整成整個畫布，就可以達到清除效果。

download()：
在html中建立< a >tag，和download屬性，並且我將檔名都預設為image.png。

upload()：
在html中我其實是命名為ReadImage()，ReadImage()功能就是先用FileReader讀取圖片，再由ctx.drawImage()顯示在畫布上，放置起始點為畫布右上角。這個功能我並沒有成功實作出image覆蓋掉原本「選擇檔案」按鈕，請助教多見諒。

undo(),redo()：
做這兩個方法的核心概念是將畫布的狀態記錄下來，並且用Array儲存這些狀態(圖片)。undo為上一步，就是先把當前畫布清除，再存取array中上一個畫布的狀態(圖片)。redo為下一步，方法同undo，但是是存取array中下一個畫布的狀態。

push()：
為了undo,redo而做的function，可以將圖片放到Array中。所以舉例來說:brush()在mouseup時需要push()，就可以把完成brush功能的狀態(圖片)記錄起來。
在triangle(),circle(),rectangle()中因為要顯示建圖過程，我將此三個function的mousemove過程的前面加上Undo()，後面加上Push()，就可以在過程中一直顯示出狀態，但不會占用到存取狀態的Array。

triangle()：
這個function與brush不同的是線的連結方式。我的作法是將mousedown的位置當第一個點，mouseup的位置當第二個點，將第一個點的X減去「二減一X的差」，Y加上「二減一Y的差」，就可以得到第三個點，將此三點連起即可得到三角形。

circle()：
我將mousedown的位置設為圓心，mouseup的位置與圓心距離設為半徑，再用ctx.arc()將圖畫出即可。

rectangle()：矩形(長方形)只需將兩個點的位置的X,Y值做排交換，就可以得到另外兩個點，再用lineto()將線連起來形成矩形。

**額外功能：**
彩虹筆：
本質上跟brush一樣，但是需要在過程中更改顏色。做法為使用hsl函式，hsl為css中與顏色有關的函式，在mousemove過程中更新hsl函式內的參數，並將其賦值給ctx.strokeStyle()，也就是筆刷的顏色，就可以達成彩色效果。

button小功能：
在滑鼠移到button上時，該button的image會有淡出效果，做法為在css中更改opacity。






### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
https://[106062372].gitlab.io/AS_01_WebCanvas


### Others (Optional)

    Anything you want to say to TAs.
謝謝助教們這麼辛苦的批改我們平時的lab和作業，還有在討論區回答我們的問題。其實有一個小小願望，也就是lab第一階段截止時間的延長，雖然助教們上次已經和教授討論過了，還是希望可以有機會延長。
還有一件事情，特別謝謝改我這份作業的助教，因為我的實作能力一直不是我的強項，寫出code沒有很好讀readme也很長，看這份作業想必不是件容易事吧！但我真的花了很多時間和精力在這份作業上面，期望這學期修完實力能得到進步，同時也能在這門課拿到高分。

<style>
table th{
    width: 100%;
}
</style>